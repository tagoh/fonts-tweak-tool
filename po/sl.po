#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://gitlab.com/tagoh/-/issues\n"
"POT-Creation-Date: 2022-11-17 15:30+0900\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: Slovenian\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Zanata 4.4.5\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || "
"n%100==4 ? 3 : 0)\n"

#: data/chooser.ui:76
msgid "Select an item to add"
msgstr ""

#: data/fonts-tweak-alias.ui:108 data/fonts-tweak-alias.ui:109
#: data/fonts-tweak-alias.ui:110 data/fonts-tweak-lang.ui:75
msgid "Add a language"
msgstr ""

#: data/fonts-tweak-alias.ui:125 data/fonts-tweak-alias.ui:126
#: data/fonts-tweak-alias.ui:127 data/fonts-tweak-lang.ui:89
msgid "Remove the language"
msgstr ""

#: data/fonts-tweak-alias.ui:192
msgid "Do not filter a font family name by classification"
msgstr ""

#: data/fonts-tweak-alias.ui:231
msgid "List a localized family name if available"
msgstr ""

#: data/fonts-tweak-alias.ui:271 data/fonts-tweak-prop.ui:258
msgid "<b>General</b>"
msgstr ""

#: data/fonts-tweak-alias.ui:310
msgid "Sans Serif:"
msgstr ""

#: data/fonts-tweak-alias.ui:328
msgid "Serif:"
msgstr ""

#: data/fonts-tweak-alias.ui:346
msgid "Monospace:"
msgstr ""

#: data/fonts-tweak-alias.ui:364
msgid "Cursive:"
msgstr ""

#: data/fonts-tweak-alias.ui:382
msgid "Fantasy:"
msgstr ""

#: data/fonts-tweak-alias.ui:400 data/fonts-tweak-alias.ui:442
#: data/fonts-tweak-alias.ui:484 data/fonts-tweak-alias.ui:526
#: data/fonts-tweak-alias.ui:568 data/fonts-tweak-alias.ui:834
#: data/fonts-tweak-alias.ui:876
msgid "Sample:"
msgstr ""

#: data/fonts-tweak-alias.ui:417 data/fonts-tweak-alias.ui:459
#: data/fonts-tweak-alias.ui:501 data/fonts-tweak-alias.ui:543
#: data/fonts-tweak-alias.ui:585 data/fonts-tweak-alias.ui:851
#: data/fonts-tweak-alias.ui:893
msgid "The quick brown fox jumps over the lazy dog. 1234567890"
msgstr ""

#: data/fonts-tweak-alias.ui:745
msgid "Emoji:"
msgstr ""

#: data/fonts-tweak-alias.ui:789
msgid "Math:"
msgstr ""

#: data/fonts-tweak-alias.ui:938
msgid "<b>Aliases</b>"
msgstr ""

#: data/fonts-tweak-alias.ui:973
msgid "Please select a language."
msgstr ""

#: data/fonts-tweak-config.ui:27
msgid "Show items enabled by system"
msgstr ""

#: data/fonts-tweak-lang.ui:18
msgid "Add languages you prefer to see."
msgstr ""

#: data/fonts-tweak-lang.ui:103
msgid "Move the selected language up"
msgstr ""

#: data/fonts-tweak-lang.ui:117
msgid "Move the selected language down"
msgstr ""

#: data/fonts-tweak-prop.ui:26 data/fonts-tweak-prop.ui:554
msgid "None"
msgstr ""

#: data/fonts-tweak-prop.ui:30
msgid "Slight"
msgstr ""

#: data/fonts-tweak-prop.ui:34
msgid "Medium"
msgstr ""

#: data/fonts-tweak-prop.ui:38
msgid "Full"
msgstr ""

#: data/fonts-tweak-prop.ui:99 data/fonts-tweak-prop.ui:100
#: data/fonts-tweak-prop.ui:101
msgid "Add a font"
msgstr ""

#: data/fonts-tweak-prop.ui:116 data/fonts-tweak-prop.ui:117
#: data/fonts-tweak-prop.ui:118
msgid "Remove the font"
msgstr ""

#: data/fonts-tweak-prop.ui:180
msgid "Use sub-pixel rendering"
msgstr ""

#: data/fonts-tweak-prop.ui:233
msgid "Use embedded bitmap font if any"
msgstr ""

#: data/fonts-tweak-prop.ui:290
msgid "Do not use any hinting data"
msgstr ""

#: data/fonts-tweak-prop.ui:318
msgid "Use hinting data in the font"
msgstr ""

#: data/fonts-tweak-prop.ui:335
msgid "Use automatic-hinting feature"
msgstr ""

#: data/fonts-tweak-prop.ui:406
msgid "<b>Hinting</b>"
msgstr ""

#: data/fonts-tweak-prop.ui:462
msgid "<b>Features</b>"
msgstr ""

#: data/fonts-tweak-prop.ui:497 data/fonts-tweak-subst.ui:361
msgid "Please select a font."
msgstr ""

#: data/fonts-tweak-prop.ui:558
msgid "Grayscale"
msgstr ""

#: data/fonts-tweak-prop.ui:562
msgid "RGB"
msgstr ""

#: data/fonts-tweak-prop.ui:566
msgid "BGR"
msgstr ""

#: data/fonts-tweak-prop.ui:570
msgid "VRGB"
msgstr ""

#: data/fonts-tweak-prop.ui:574
msgid "VBGR"
msgstr ""

#: data/fonts-tweak-subst.ui:65
msgid "Select a font to add"
msgstr ""

#: data/fonts-tweak-subst.ui:186 data/fonts-tweak-subst.ui:187
msgid "Add a substitute font"
msgstr ""

#: data/fonts-tweak-subst.ui:201 data/fonts-tweak-subst.ui:202
msgid "Remove the substitute font"
msgstr ""

#: data/fonts-tweak-subst.ui:278 data/fonts-tweak-subst.ui:279
msgid "Add a font to the substitute font"
msgstr ""

#: data/fonts-tweak-subst.ui:293 data/fonts-tweak-subst.ui:294
msgid "Remove the font from the substitute font"
msgstr ""

#: data/fonts-tweak-subst.ui:308 data/fonts-tweak-subst.ui:309
msgid "Move the selected font up"
msgstr ""

#: data/fonts-tweak-subst.ui:323 data/fonts-tweak-subst.ui:324
msgid "Move the selected font down"
msgstr ""

#: data/menu.ui:7
msgid "_Help"
msgstr ""

#: data/menu.ui:11
msgid "_About"
msgstr ""

#: data/menu.ui:15
msgid "_Quit"
msgstr ""

#: fonts-tweak-tool.desktop.in:3 fontstweak/main.py:74
msgid "Fonts Tweak Tool"
msgstr ""

#: fonts-tweak-tool.desktop.in:4
msgid "Tweak fonts by language using fontconfig"
msgstr ""

#: fontstweak/main.py:73
msgid "About Fonts Tweak Tool"
msgstr ""

#: fontstweak/main.py:112
msgid "Fonts Tweaks"
msgstr ""

#: fontstweak/util.py:65
msgid "Default"
msgstr ""

#: fontstweak/ui/fonts-tweak-alias.py:53
msgid "Fonts Aliases"
msgstr ""

#: fontstweak/ui/fonts-tweak-alias.py:103 fontstweak/ui/fonts-tweak-lang.py:84
msgid "Choose a language..."
msgstr ""

#: fontstweak/ui/fonts-tweak-config.py:84
msgid "enabled by system"
msgstr ""

#: fontstweak/ui/fonts-tweak-config.py:86
msgid "user-defined file"
msgstr ""

#: fontstweak/ui/fonts-tweak-config.py:105
msgid "Fontconfig recipes"
msgstr ""

#: fontstweak/ui/fonts-tweak-lang.py:45
msgid "Language Ordering"
msgstr ""

#: fontstweak/ui/fonts-tweak-prop.py:84
msgid "Fonts Properties"
msgstr ""

#: fontstweak/ui/fonts-tweak-prop.py:148 fontstweak/ui/fonts-tweak-subst.py:73
#: fontstweak/ui/fonts-tweak-subst.py:90
msgid "Select a font..."
msgstr ""

#: fontstweak/ui/fonts-tweak-subst.py:48
msgid "Fonts Substitutions"
msgstr ""
