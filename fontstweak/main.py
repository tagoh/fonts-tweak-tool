# -*- coding: utf-8 -*-
# main.py
# Copyright (C) 2012-2023 Red Hat, Inc.

# Authors:
#   Akira TAGOH  <tagoh@redhat.com>

# This library is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio  # noqa: E402
try:
    from util import FontsTweakUtil, _
except ImportError:
    from fontstweak.util import FontsTweakUtil, _
try:
    from tweaks import FontsTweak
except ImportError:
    from fontstweak.tweaks import FontsTweak


class FontsTweakToolApp(Gtk.Application):

    def __init__(self, config):
        Gtk.Application.__init__(self,
                                 application_id='org.bitbucket.FontsTweakTool')
        self.window = None
        self.config = config

    def do_startup(self):
        Gtk.Application.do_startup(self)

        self.builder = FontsTweakUtil.create_builder('menu.ui')
        self.set_app_menu(self.builder.get_object('menu'))

        help_action = Gio.SimpleAction.new('help', None)
        help_action.connect('activate', self.help_cb)
        self.add_action(help_action)
        about_action = Gio.SimpleAction.new('about', None)
        about_action.connect('activate', self.about_cb)
        self.add_action(about_action)
        quit_action = Gio.SimpleAction.new('quit', None)
        quit_action.connect('activate', self.quit_cb)
        self.add_action(quit_action)

    def do_activate(self, data=None):
        self.window = FontsTweakToolMainWindow(self, self.config)
        self.window.show()
        self.window.present()
        self.add_window(self.window)

    def do_delete_event(self, data=None):
        Gtk.main_quit()

    def help_cb(self, action, params):
        pass

    def about_cb(self, action, params):
        d = Gtk.AboutDialog()
        d.set_title(_("About Fonts Tweak Tool"))
        d.set_program_name(_("Fonts Tweak Tool"))
        d.set_copyright(("Copyright © 2011-2012 Jian Ni <jni@redhat.com>\n"
                         "Copyright © 2012-2023 Red Hat, Inc."))
        d.set_logo_icon_name('fonts-tweak-tool')
        d.set_website("http://bitbucket.org/tagoh/fonts-tweak-tool")
        d.set_license_type(Gtk.License.LGPL_3_0)
        d.set_authors(
            ['Jian Ni <jni@redhat.com>', 'Akira TAGOH <tagoh@redhat.com>'])
        d.connect('response', lambda w, r: d.destroy())
        d.show()

    def quit_cb(self, action, params):
        self.quit()


class FontsTweakToolMainWindow(Gtk.ApplicationWindow):

    def __init__(self, app, config):
        Gtk.ApplicationWindow.__init__(self, application=app)
        self.sizegroup = Gtk.SizeGroup.new(Gtk.SizeGroupMode.HORIZONTAL)
        self.set_titlebar(self._get_titlebar())
        self.config = config

        hbox = Gtk.HBox()
        hbox.pack_start(self._get_main_content(), True, True, 0)
        hbox.show()
        self.add(hbox)
        self.show()

        FontsTweakUtil.load_cssfile('fonts-tweak-tool.css')

    def _get_titlebar(self):
        header = Gtk.Box()
        lheader = Gtk.HeaderBar()
        lheader.get_style_context().add_class('titlebar')
        lheader.get_style_context().add_class('fft-titlebar-left')
        rheader = Gtk.HeaderBar()
        rheader.props.show_close_button = True
        rheader.get_style_context().add_class('titlebar')
        rheader.get_style_context().add_class('fft-titlebar-right')

        w = Gtk.Label.new(_('Fonts Tweaks'))
        w.get_style_context().add_class('title')
        lheader.set_custom_title(w)

        self.hbox = Gtk.HBox()
        self.hbox.get_style_context().add_class('tab')
        rheader.set_custom_title(self.hbox)

        header.pack_start(lheader, False, False, 0)
        header.pack_start(Gtk.VSeparator(), False, False, 0)
        header.pack_start(rheader, True, True, 0)

        self.sizegroup.add_widget(lheader)
        header.show_all()

        return header

    def _get_main_content(self):
        self.stack = Gtk.Stack()
        self.stack.props.margin = 20
        self.stack.show()
        ui = self._load_tweaks()
        for u in sorted(ui.keys()):
            if ui[u].is_enabled():
                ui[u].set_config(self.config)
                self.stack.add_titled(ui[u], u, _(ui[u].info.title))
        self.switcher = Gtk.StackSwitcher()
        self.switcher.set_stack(self.stack)
        self.switcher.show_all()
        self.hbox.pack_start(self.switcher, False, False, 0)

        return self.stack

    def _load_tweaks(self):
        return FontsTweak.load_tweaks()

    def _on_row_selected(self, listbox, row):
        if row:
            s = row.get_child().get_text()
            self.stack.set_visible_child_name(s)
            self.title.set_text(s)

    def _on_tab_toggled(self, button):
        pass
