# -*- coding: utf-8 -*-
# fonts-tweak-alias.py
# Copyright (C) 2012-2023 Red Hat, Inc.
#
# Authors:
#   Akira TAGOH  <tagoh@redhat.com>
#
# This library is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
try:
    from tweaks import FontsTweak
except ImportError:
    from fontstweak.tweaks import FontsTweak
try:
    from chooserui import FontsTweakChooserUI
except ImportError:
    from fontstweak.chooserui import FontsTweakChooserUI
try:
    from util import FontsTweakUtil, _, N_
except ImportError:
    from fontstweak.util import FontsTweakUtil, _, N_
from gi.repository import GLib
from gi.repository import GObject
from gi.repository import Gtk
from gi.repository import Easyfc
from gi.repository import Pango
from xml.sax.saxutils import quoteattr
from xml.sax.saxutils import escape


class FontsTweakAliasUI(Gtk.Box, FontsTweak):

    alias_names = [
        'sans-serif', 'serif', 'monospace', 'cursive', 'fantasy', 'emoji',
        'math'
    ]

    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        FontsTweak.__init__(self, 'alias', N_('Fonts Aliases'))
        self.config = None
        builder = FontsTweakUtil.create_builder('fonts-tweak-alias.ui')
        builder.connect_signals(self)
        w = builder.get_object('fonts-alias-ui')
        self.pack_start(w, True, True, 0)
        self.show_all()

        self.remove_button = builder.get_object('toolbutton-remove-alias-lang')
        self.pages = builder.get_object('notebook-aliases-pages')
        self.selector = builder.get_object('treeview-selection')
        self.view = builder.get_object('treeview-alias-lang-list')
        self.view.append_column(
            Gtk.TreeViewColumn(None, Gtk.CellRendererText(), text=0))
        self.view_list = builder.get_object('alias-lang-list')
        self.classification_filter = builder.get_object('switch-filter')
        self.localized_name = builder.get_object('switch-localized-name')

        # check if current icon theme supports the symbolic icons
        add_icon = builder.get_object('toolbutton-add-alias-lang')
        add_icon.set_icon_name(
            FontsTweakUtil.check_symbolic(add_icon.get_icon_name()))
        del_icon = builder.get_object('toolbutton-remove-alias-lang')
        del_icon.set_icon_name(
            FontsTweakUtil.check_symbolic(del_icon.get_icon_name()))

        try:
            Easyfc.version()
            if len(Easyfc.Font.get_list('en', 'sans-serif', False)) == 0:
                raise AttributeError
        except AttributeError:
            self.localized_name.set_active(True)
            self.localized_name.set_sensitive(False)
        self.comboboxes = {}
        self.labels = {}
        self.lists = {}
        self.fonts = {}
        for f in self.alias_names:
            self.comboboxes[f] = builder.get_object('combobox-' + f)
            self.lists[f] = builder.get_object(f + '-fonts-list')
            self.labels[f] = builder.get_object('label-sample-' + f)

        self.listobj = Gtk.ListStore(GObject.TYPE_STRING, GObject.TYPE_STRING)

        self.langlist = FontsTweakUtil.get_language_list(True)
        for ll in self.langlist.keys():
            iter = self.listobj.append()
            self.listobj.set_value(iter, 0, ll)
            self.listobj.set_value(iter, 1, self.langlist[ll])

        chooser_builder = FontsTweakUtil.create_builder('chooser.ui')
        chooser_builder.connect_signals(
            FontsTweakChooserUI(chooser_builder, self.listobj,
                                self.on_treemodel_filter))
        self.chooser = chooser_builder.get_object('chooser-dialog')
        self.chooser.set_title(_('Choose a language...'))
        self.chooser_view = chooser_builder.get_object('treeview')
        self.chooser_selector = chooser_builder.get_object(
            'treeview-selection')
        self.chooser_view.append_column(
            Gtk.TreeViewColumn(None, Gtk.CellRendererText(), text=1))

    def is_enabled(self):
        return True

    def __font_changed(self, widget, alias):
        if self.__initialized is False:
            return
        model, iter = self.selector.get_selected()
        if iter is not None:
            lang = model.get_value(iter, 1)
            model = widget.get_model()
            iter = widget.get_active_iter()
            if iter is not None:
                font = model.get_value(iter, 0)
                self.config.remove_alias(lang, alias)
                if font not in self.alias_names:
                    a = Easyfc.Alias.new(alias)
                    if self.classification_filter.get_active():
                        a.check_font_existence(False)
                    try:
                        a.set_font(font)
                        self.config.add_alias(lang, a)
                    except GLib.GError:
                        pass
                try:
                    self.config.save()
                except GLib.GError as e:
                    if e.domain != 'ezfc-error-quark' and e.code != 6:
                        raise
                self.__render_label(widget, lang)

    def on_treemodel_filter(self, model, iter, filter):
        patterns = filter.get_text().split(' ')
        if len(patterns) == 1 and patterns[0] == '':
            return True
        t, n = model.get(iter, 0, 1)
        for p in patterns:
            if re.search(p, n, re.I):
                return True
            if re.search(p, t, re.I):
                return True

        return False

    def on_switch_filter_active(self, widget, gparams):
        self.on_treeview_selection_changed(self.selector)

    def on_switch_localized_name_active(self, widget, gparams):
        self.on_treeview_selection_changed(self.selector)

    def on_combobox_sans_serif_changed(self, widget, *args):
        self.__font_changed(widget, 'sans-serif')

    def on_combobox_serif_changed(self, widget, *args):
        self.__font_changed(widget, 'serif')

    def on_combobox_monospace_changed(self, widget, *args):
        self.__font_changed(widget, 'monospace')
        pass

    def on_combobox_cursive_changed(self, widget, *args):
        self.__font_changed(widget, 'cursive')
        pass

    def on_combobox_fantasy_changed(self, widget, *args):
        self.__font_changed(widget, 'fantasy')

    def on_combobox_emoji_changed(self, widget, *args):
        self.__font_changed(widget, 'emoji')

    def on_combobox_math_changed(self, widget, *args):
        self.__font_changed(widget, 'math')

    def on_treeview_selection_changed(self, widget, *args):
        model, iter = widget.get_selected()
        if iter is None:
            self.pages.set_current_page(1)
            self.remove_button.set_sensitive(False)
        else:
            lang = model.get_value(iter, 1)
            for n in self.alias_names:
                self.__render_combobox(lang, n)
            self.pages.set_current_page(0)
            self.remove_button.set_sensitive(True)
            if lang != '':
                self.comboboxes['emoji'].set_sensitive(False)
                self.comboboxes['math'].set_sensitive(False)
            else:
                self.comboboxes['emoji'].set_sensitive(True)
                self.comboboxes['math'].set_sensitive(True)

    def on_toolbutton_add_alias_lang_clicked(self, widget):
        if self.get_toplevel() is not None:
            self.chooser.set_transient_for(self.get_toplevel())

        self.chooser.show_all()
        resid = self.chooser.run()
        self.chooser.hide()
        if resid == Gtk.ResponseType.CANCEL:
            return
        model, iter = self.chooser_selector.get_selected()
        if iter is None:
            return
        tag, name = model.get(iter, 0, 1)
        iter = self.add_language(name, tag)
        if iter is None:
            print("%s has already been added." % tag)
        else:
            model = self.view.get_model()
            path = model.get_path(iter)
            self.view.set_cursor(path, None, False)

    def on_toolbutton_remove_alias_lang_clicked(self, widget):
        model, iter = self.selector.get_selected()
        if iter is None:
            return
        lang = model.get_value(iter, 1)
        model.remove(iter)
        self.config.remove_aliases(lang)
        try:
            self.config.save()
        except GLib.GError as e:
            if e.domain != 'ezfc-error-quark' and e.code != 6:
                raise
        self.on_treeview_selection_changed(self.selector)

    def add_language(self, name, tag):
        retval = True
        model = self.view.get_model()
        iter = model.get_iter_first()
        while iter is not None:
            n, t = model.get(iter, 0, 1)
            if t == tag:
                retval = False
                break
            iter = model.iter_next(iter)
        if retval is True:
            iter = model.append()
            model.set_value(iter, 0, name)
            model.set_value(iter, 1, tag)
        else:
            iter = None
        return iter

    def __render_combobox(self, lang, alias):
        if lang not in self.fonts:
            self.fonts[lang] = {}
        kalias = None if self.classification_filter.get_active() else alias
        if kalias not in self.fonts[lang]:
            self.fonts[lang][kalias] = {}
        flocalized = self.localized_name.get_active()
        if flocalized not in self.fonts[lang][kalias]:
            self.fonts[lang][kalias][flocalized] = Easyfc.Font.get_list(
                lang, kalias, flocalized)
        if len(self.fonts[lang][kalias][flocalized]) == 0:
            # fontconfig seems not supporting the namelang object
            flocalized = True
            self.fonts[lang][kalias][flocalized] = Easyfc.Font.get_list(
                lang, kalias, flocalized)
        self.lists[alias].clear()
        self.lists[alias].append([alias])
        for f in self.fonts[lang][kalias][flocalized]:
            self.lists[alias].append([f])
        fn = None
        for a in self.config.get_aliases(lang):
            if a.get_name() == alias:
                fn = a.get_font()
                break
        if fn is not None:
            model = self.comboboxes[alias].get_model()
            iter = model.get_iter_first()
            while iter is not None:
                try:
                    f = unicode(model.get_value(iter, 0), "utf8")
                    if type(fn) is not unicode:
                        fontname = unicode(fn, "utf8")
                except NameError:
                    f = model.get_value(iter, 0)
                    fontname = fn
                if f == fontname:
                    self.comboboxes[alias].set_active_iter(iter)
                    break
                iter = model.iter_next(iter)
        else:
            self.comboboxes[alias].set_active(0)
        self.__render_label(self.comboboxes[alias], lang)

    def __render_label(self, combobox, lang):
        model = combobox.get_model()
        iter = combobox.get_active_iter()
        if iter is not None:
            font = model.get_value(iter, 0)
            # Work around for PyGObject versions 3.0.3 and 3.1.0,
            # which decode strings in Gtk.TreeModel when retrieval.
            # This behavior was reverted in 3.1.1:
            # http://git.gnome.org/browse/pygobject/commit/?id=0285e107
            try:
                if type(font) is not unicode:
                    font = unicode(font, "utf8")
            except NameError:
                pass
            iter = model.get_iter_first()
            alias = model.get_value(iter, 0)
            try:
                sample = unicode(
                    Pango.Language.get_sample_string(
                        Pango.Language.from_string(lang)), 'utf-8')
            except NameError:
                sample = Pango.Language.get_sample_string(
                    Pango.Language.from_string(lang))
            self.labels[alias].set_markup(
                "<span font_family=%s font_size=\"small\">%s</span>" %
                (quoteattr(font), escape(sample)))

    def do_set_config(self):
        self.__initialized = False
        for ll in self.config.get_language_list():
            if ll not in self.langlist:
                print("%s is an unknown language tag. ignoring." % ll)
                continue
            desc = self.langlist[ll]
            self.add_language(desc, ll)
            for a in self.config.get_aliases(ll):
                an = a.get_name()
                if an not in self.alias_names:
                    print("{} is unexpected alias name. ignoring.".format(an))
                    self.config.remove_alias(None, an)
                else:
                    self.__render_combobox(ll, an)

        self.on_treeview_selection_changed(self.selector)

        self.__initialized = True


TWEAKS_UI = [FontsTweakAliasUI()]
