# -*- coding: utf-8 -*-
# fonts-tweak-config.py
# Copyright (C) 2012-2018 Red Hat, Inc.
#
# Authors:
#   Akira TAGOH  <tagoh@redhat.com>
#
# This library is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import collections
import gi
import os
import sys
from distutils.version import LooseVersion
from gi.repository import Gtk
from gi.repository import Pango
from gi.repository import Easyfc
try:
    from tweaks import FontsTweak
except ImportError:
    from fontstweak.tweaks import FontsTweak
try:
    from util import FontsTweakUtil
except ImportError:
    from fontstweak.util import FontsTweakUtil

def N_(str): return str

class FontsTweakConfigItem(Gtk.ListBoxRow):

    def __init__(self, config, data = None):
        self.config = config

        data = self
        Gtk.ListBoxRow.__init__(self)
        self.box = Gtk.HBox()
        self.box.props.border_width = 10
        self.box.props.spacing = 4
        self.box.get_style_context().add_class('config-item')
        self.label = Gtk.Label.new("")
        self.label.props.ellipsize = Pango.EllipsizeMode.END
        self.label.props.xalign = 0.0
        self.desc = Gtk.Label.new("")
        self.desc.props.ellipsize = Pango.EllipsizeMode.END
        self.desc.props.xalign = 0.0
        self.note = Gtk.Label.new("")
        self.note.props.ellipsize = Pango.EllipsizeMode.END
        self.note.props.xalign = 0.0
        self.sw = Gtk.Switch()
        self.sw.props.vexpand = False
        self.sw.props.valign = Gtk.Align.CENTER
        self.box.pack_start(self.sw, False, False, 0)
        self.box.pack_start(self.label, True, True, 10)
        self.box.pack_start(self.desc, True, True, 10)
        self.box.pack_start(self.note, True, True, 10)
        self.update()
        self.label.set_label(self.get_shortname())
        self.sw.connect('notify::active', self.on_switch_activated, data)

        self.add(self.box)

    def get_name(self):
        return self.config.get_name()

    def get_shortname(self):
        return os.path.basename(self.get_name())

    def is_enabled(self):
        return self.config.is_enabled()

    def update(self):
        if self.config.is_system_conf() and self.is_enabled():
            self.note.set_label('[' + _('enabled by system') + ']')
        elif self.config.is_user_conf():
            self.note.set_label('[' + _('user-defined file') + ']')
        self.desc.set_label(self.config.get_description())
        self.sw.set_active(self.is_enabled())
        self.sw.set_sensitive(not (self.config.is_system_conf() or self.config.is_user_conf()))

    def analyze(self, path):
        if os.path.islink(path):
            return os.path.realpath(path)
        return None

    def on_switch_activated(self, sw, gparams, data):
        if not self.config.is_system_conf():
            self.config.set_enable(sw.get_active())
        self.update()

class FontsTweakConfigUI(Gtk.Box, FontsTweak):

    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        FontsTweak.__init__(self, 'config', N_('Fontconfig recipes'))

        builder = FontsTweakUtil.create_builder('fonts-tweak-config.ui')
        builder.connect_signals(self)
        w = builder.get_object('fonts-config-ui')
        self.pack_start(w, True, True, 0)
        self.show_all()

        self.scrolled_window = builder.get_object('scrolledwindow-config-list')
        self.sw_enabled_by_system = builder.get_object('switch-enabled-by-system')
        self.sizegroup = Gtk.SizeGroup.new(Gtk.SizeGroupMode.HORIZONTAL)
        self.desc_sizegroup = Gtk.SizeGroup.new(Gtk.SizeGroupMode.HORIZONTAL)
        self.note_sizegroup = Gtk.SizeGroup.new(Gtk.SizeGroupMode.HORIZONTAL)

        self.listbox = Gtk.ListBox()
        self.listbox.get_style_context().add_class('config-list')
        self.scrolled_window.add(self.listbox)
        self.scrolled_window.show_all()
        self.items = {}

        self.config = []
        try:
            self.config = Easyfc.FontConfig.get_list()
        except AttributeError:
            pass
        for i in self.config:
            item = FontsTweakConfigItem(i)
            if item.get_name() in self.items:
                print("duplicate config for %s" % i.get_name())
                continue
            self.items[item.get_name()] = item

        d = collections.OrderedDict(sorted(self.items.items()))
        for (k, v) in d.items():
            self.sizegroup.add_widget(v.label)
            self.desc_sizegroup.add_widget(v.desc)
            self.note_sizegroup.add_widget(v.note)
            v.show_all()
            self.listbox.add(v)
        self.on_switch_enabled_by_system_active(self.sw_enabled_by_system, [])

    def is_enabled(self):
        try:
            ezfcver = Easyfc.version()
            if LooseVersion(ezfcver) >= LooseVersion('0.14.0'):
                return True
        except AttributeError:
            pass

        return False

    def on_switch_enabled_by_system_active(self, sw, gparams):
        for (k, v) in self.items.items():
            if v.config.is_system_conf():
                if not self.sw_enabled_by_system.get_active():
                    v.set_visible(False)
                else:
                    v.set_visible(True)

    def do_set_config(self):
        pass

TWEAKS_UI = [FontsTweakConfigUI()]
