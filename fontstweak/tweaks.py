# -*- coding: utf-8 -*-
# tweaks.py
# Copyright (C) 2012-2023 Red Hat, Inc.
#
# Authors:
#   Akira TAGOH  <tagoh@redhat.com>
#
# This library is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import glob


class FontsTweak(object):
    _tweaks = {}

    @classmethod
    def load_tweaks(self):
        ui = []

        path = os.path.dirname(__file__)
        files = [
            os.path.splitext(os.path.split(f)[-1])[0]
            for f in glob.glob(os.path.join(path, 'ui', 'fonts-tweak-*.py'))
        ]
        mods = __import__('fontstweak.ui', globals(), locals(), files, 0)
        for m in [getattr(mods, f) for f in files]:
            ui.extend(getattr(m, 'TWEAKS_UI', []))
        for u in ui:
            FontsTweak._tweaks[u.info.identifier] = u

        return FontsTweak._tweaks

    def __init__(self, name, title):
        self.info = TweaksInfo(name, title)
        self.config = None

    def set_config(self, config):
        self.config = config
        self.do_set_config()

    def do_set_config(self):
        pass


class TweaksInfo(object):

    def __init__(self, name, title):
        self.title = title
        self.identifier = name
